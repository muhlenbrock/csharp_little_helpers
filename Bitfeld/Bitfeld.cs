﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace muhlenbrock
{
    public class Bitfeld
    {
        public uint state { get; private set; }
        public int length = 8;
        public Dictionary<string,int> options { get; set; }

        /// <summary>
        /// Initialisiert ein leeres Bitfeld mit dem Wert 0
        /// </summary>
        public Bitfeld(uint state=0)
        {
            init(null, state);
        }
        public Bitfeld(string[] options)
        {
            init(options);
        }
        public Bitfeld(string[] option, uint state)
        {
            init(option, state);
        }

        private void init(string[] options = null, uint state = 0)
        {
            this.state = state;
            if (options != null)
            {
                this.options = new Dictionary<string, int>();
                for (int i = 0; i < options.Count(); i++)
                {
                    this.options.Add(options[i], i);
                }
            }
        }


        private void LengthCheck(int index)
        {
            if (index >= length) { throw new System.IndexOutOfRangeException(); }
        }
        /// <summary>
        /// Ermittelt den Wert des Bits an der Stelle Index
        /// </summary>
        /// <param name="index">Bitnummer</param>
        /// <returns>bool passend zum gefundenen Bit</returns>
        public bool get(int index)
        {
            LengthCheck(index);
            uint i = 1u << index;
            return (state & i) == i;
        }
        public bool get(string option)
        {
            return get(options[option]);
        }
        /// <summary>
        /// setzt das Bit an der Stelle index auf 1
        /// </summary>
        /// <param name="index">Bitnummer</param>
        public void set(int index)
        {
            LengthCheck(index);
            uint i = 1u << index;
            state = state | i;
        }
        public void set(string option)
        {
            set(options[option]);
        }

        /// <summary>
        /// setzt das Bit an der Stelle index auf 0
        /// </summary>
        /// <param name="index">Bitnummer</param>
        public void unset(int index)
        {
            LengthCheck(index);
            uint i = 1u << index;
            state = state & (~i);
        }
        public void unset(string option)
        {
            unset(options[option]);
        }

        /// <summary>
        /// Schaltet das Bit an der Stelle index zwischen 0 und 1 um.
        /// </summary>
        /// <param name="index">Bitnummer</param>
        public void toggle(int index)
        {
            LengthCheck(index);
            uint i = 1u << index;
            state = state ^ i;
        }
        public void toggle(string option)
        {
            toggle(options[option]);
        }

        /// <summary>
        /// Gibt den Dezimalwert des Bitfelds aus.
        /// </summary>
        /// <returns></returns>
        public uint getState()
        {
            return state;
        }

        /// <summary>
        /// Liefert eine binäre Darstellung der Bits im Bitfeld. Achtung: Diese ist in der Leserichtung genau anders herum als die binäre Darstellung einer Zahl.
        /// </summary>
        /// <returns></returns>
        public string getBits()
        {
            int cursor = 0;
            string display = string.Empty;
            while (cursor < length)
            {
                display += get(cursor) ? "1" : "0";
                cursor++;
            }
            return display;
        }

        /// <summary>
        /// Liefert true, wenn kein Bit gesetzt ist.
        /// </summary>
        /// <returns></returns>
        public bool none()
        {
            return state == 0;
        }

        /// <summary>
        /// Liefert true, wenn mindestens ein Bit gesetzt ist.
        /// </summary>
        /// <returns></returns>
        public bool any()
        {
            return state > 0;
        }

        /// <summary>
        /// liefert true, wenn alle Bits bis zu einem Grenzwert gesetzt sind.
        /// </summary>
        /// <returns></returns>
        public bool all()
        {
            uint max = (2u << (length-1)) - 1;
            return state == max;
        }

        /// <summary>
        /// leert das Bitfeld
        /// </summary>
        public void clear()
        {
            state = 0;
        }

        /// <summary>
        /// Füllt das Bitfeld mit einsen auf.
        /// </summary>
        public void fill()
        {
            state = (2u << (length-1)) - 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using muhlenbrock;

namespace testgebiet
{
    class Program
    {
        private static int ReadI()
        {
            Console.Write("Nummer? > ");
            return Convert.ToInt32(Console.ReadLine());
        }
        private static void ConsoleHR()
        {
            Console.WriteLine("------------------------------------------------------------");
        }

        private static string ReadOpt()
        {
            Console.Write("Name? > ");
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {
            List<string> options = new List<string>();
            string opt = string.Empty;
            do
            {
                Console.Write("Optionsname oder x für Abbruch: >> ");
                opt = Console.ReadLine();
                if (opt != "x") options.Add(opt);
            } while (opt != "x");
            string next = string.Empty;
            Bitfeld Bits = options.Count > 0 ? new Bitfeld(options.ToArray()) : new Bitfeld();
            Bits.length = 4;
            while (next != "x")
            {
                ConsoleHR();
                Console.WriteLine("Wähle eine Aktion:");
                Console.Write("(s)etzen\r\n (l)esen\r\n a(u)sschalten\r\n umschal(t)en\r\n (z)ahl\r\n (b)inärcode\r\n (a)lle\r\n kei(n)e\r\n lös(c)en\r\n (f)üllen\r\n o(p)tion setzen\r\n(o)ption suchen\r\n e(x)it >> ");
                next = Console.ReadLine();
                ConsoleHR();
                try
                {
                    switch (next)
                    {
                        case "s": int i = ReadI(); Bits.set(i); break;
                        case "l": i = ReadI(); Console.WriteLine(Bits.get(i) ? "true" : "false"); break;
                        case "u": i = ReadI(); Bits.unset(i); break;
                        case "t": i = ReadI(); Bits.toggle(i); break;
                        case "z": Console.WriteLine(Bits.getState()); break;
                        case "b": Console.WriteLine(Bits.getBits()); break;
                        case "a": Console.WriteLine(Bits.all() ? "voll" : "nicht voll"); break;
                        case "n": Console.WriteLine(Bits.any() ? "ein wert ist da..." : "kein wert ist da"); break;
                        case "c": Bits.clear(); break;
                        case "f": Bits.fill(); break;
                        case "o": Console.WriteLine(Bits.get(ReadOpt())); break;
                        case "p": Bits.set(ReadOpt());break;

                        default: break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Zahl liegt nicht im Bitfeld.");
                }
            }
            
        }
    }
}
